import { Db, MongoClient } from 'mongodb';
import { config } from '../src/config';

let mongodb = null;

export async function getDatabase(): Promise<Db> {
    if (mongodb) { return mongodb; }

    const mongoDBURL = (config.get('db.auth.user') && config.get('db.auth.password'))
        ? `mongodb://${config.get('db.auth.user')}:${config.get('db.auth.password')}@${config.get('db.host')}/${config.get('db.name')}?retryWrites=true&w=majority`
        : `mongodb://${config.get('db.host')}/${config.get('db.name')}?retryWrites=true&w=majority`;

    const connection = await MongoClient.connect(mongoDBURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    mongodb = connection.db();
    return mongodb;
}
