import { dropTemplateOperations } from './helpers/reset-database';
import { insertDefaultTemplatesOperations } from './helpers/insert-default-ope-templates';
import { insertDefaultTemplatesOperationsCartAndCheckbook } from './helpers/insert-default-ope-chekbook-cart-templates';

(async () => {
    // await dropTemplateOperations(true);
    await insertDefaultTemplatesOperationsCartAndCheckbook();
    // await insertDefaultTemplatesOperations();
    process.exit();
})();