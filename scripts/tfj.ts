import { insertDefaultTfjUpdateDate } from './helpers/insert-default-last-tfj-date';
import { dropTfj } from './helpers/reset-database';

(async () => {
    await dropTfj(true);
    await insertDefaultTfjUpdateDate();
    process.exit();
})();