import { insertDefaultOperationCodeByProductCode } from './helpers/insert-default-ope-by-product-code';
import { dropOperationsByProductCode } from './helpers/reset-database';

(async () => {
    await dropOperationsByProductCode(true);
    await insertDefaultOperationCodeByProductCode();
    process.exit();
})();