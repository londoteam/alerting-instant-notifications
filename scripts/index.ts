
import { dropDatabase } from './helpers/reset-database';
import { insertDefaultAGE } from './helpers/insert-default-age';
import { insertDefaultRegions } from './helpers/insert-default-regions';
import { insertDefaultQueries } from './helpers/insert-default-queries';
import { insertDefaultSettings } from './helpers/insert-default-settings';
import { insertDefaultTfjUpdateDate } from './helpers/insert-default-last-tfj-date';
import { insertDefaultStatusSending } from './helpers/insert-default-status-sending';
import { insertDefaultTemplatesCredit } from './helpers/insert-default-credit-templates';
import { insertDefaultTemplatesOperations } from './helpers/insert-default-ope-templates';
import { insertDefaultOperationCodeByProductCode } from './helpers/insert-default-ope-by-product-code';

(async () => {
    await dropDatabase(true);
    await insertDefaultAGE();
    await insertDefaultQueries();
    await insertDefaultRegions();
    await insertDefaultSettings();
    await insertDefaultStatusSending();
    await insertDefaultTemplatesCredit();
    await insertDefaultTfjUpdateDate();
    await insertDefaultOperationCodeByProductCode();
    await insertDefaultTemplatesOperations();
    process.exit();
})();



