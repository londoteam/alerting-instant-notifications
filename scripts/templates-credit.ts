import { dropTemplateCredit } from './helpers/reset-database';
import { insertDefaultTemplatesCredit } from './helpers/insert-default-credit-templates';

(async () => {
    await dropTemplateCredit(true);
    await insertDefaultTemplatesCredit();
    process.exit();
})();