import { getDatabase } from '../config';
import { logger } from '../../src/winston';
import { config } from '../../src/config';
import { Db } from 'mongodb';

export async function insertDefaultQueries(): Promise<any> {

    const queries = [
        {
            ref: 'REQ001',
            label: 'Requête liste échéances',
            desc: `Retourne la liste des échéances des clients sur une période définie.`,
            sqlCode: ``,
            variables: [
                { name: 'CODE_AGENCE', type: 'STRING', desc: `Code de l'agence` },
                { name: 'LIBAGE', type: 'STRING', desc: `Nom de l'agence` },
                { name: 'EMAIL_DA', type: 'STRING', desc: `Email directeur de l'agence` },
                { name: 'CODE_REGION', type: 'STRING', desc: `Code de la région` },
                { name: 'LIBREG', type: 'STRING', desc: `Nom de la région` },
                { name: 'EMAIL_DR', type: 'STRING', desc: `Email directeur de la région` },
                { name: 'EMAIL_DR_ADJ_1', type: 'STRING', desc: `Email premier directeur adjoint de la région` },
                { name: 'EMAIL_DR_ADJ_2', type: 'STRING', desc: `Email deuxième directeur adjoint de la région` },
                { name: 'NUM_DOSSIER', type: 'STRING', desc: `Numéro du dossier de crédit` },
                { name: 'NUM_ECHEANCE', type: 'STRING', desc: `Numéro de l'échéance` },
                { name: 'DATE_APPEL', type: 'DATE', desc: `Date de l'échéance` },
                { name: 'TOTAL_ECH', type: 'NUMBER', desc: `Montant de l'échéance` },
                { name: 'TOTAL_A_PAYER', type: 'NUMBER', desc: `Montant total à payer pour l'échéance` },
                { name: 'TOTAL_CREDIT_A_PAYER', type: 'NUMBER', desc: `Montant total à payer pour le crédit` },
                { name: 'CODE_CLIENT', type: 'STRING', desc: `Code client` },
                { name: 'STATUT', type: 'STRING', desc: `Statut de l'échéance` },
                { name: 'NOM_CLIENT', type: 'STRING', desc: `Nom du client` },
                { name: 'PRENOM_CLIENT', type: 'STRING', desc: `Prenom du client` },
                { name: 'NOM_DU_CLIENT', type: 'STRING', desc: `Nom complet du client` },
                { name: 'SEXE_CLIENT', type: 'STRING', desc: `Sexe du client` },
                { name: 'LANGUE_CLIENT', type: 'STRING', desc: `Langue du client` },
                { name: 'TYPE_CLIENT', type: 'STRING', desc: `Type du client` },
                { name: 'PROFIL_CLIENT', type: 'STRING', desc: `Profil du client` },
                { name: 'CODOE_PROFIL_CLIENT', type: 'STRING', desc: `Profil du client` },
                { name: 'TELEPHONE_CLIENT', type: 'STRING', desc: `Numéro de téléphone client` },
                { name: 'EMAIL_CLIENT', type: 'STRING', desc: `Adresse e-mail du client` },
                { name: 'CODE_GESTIONNAIRE', type: 'STRING', desc: `Code du gestionnaire` },
                { name: 'NOM_GESTIONNAIRE', type: 'STRING', desc: `Nom du gestionnaire` },
                { name: 'EMAIL_GESTIONNAIRE', type: 'STRING', desc: `Adresse e-mail du gestionnaire` },
                { name: 'MONTANT_PRET', type: 'NUMBER', desc: `Montant total du prêt` },
                { name: 'CODE_AGE_COMPTE', type: 'NUMBER', desc: `Code agence compte` },
                { name: 'NUM_COMPTE_CLIENT', type: 'NUMBER', desc: `Numéro de compte du client` },
                { name: 'CLE_COMPTE', type: 'NUMBER', desc: `Adresse e-mail du gestionnaire` },
            ],
            dates: {
                created: 1601476686964,
                updated: 1601476686964
            }
        }
    ];

    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('queries').insertMany(queries);
        logger.info(`${result.n} documents inserted in 'queries' collection`);
    } catch (error) {
        logger.error(`default queries insertion failed \n${error.stack}`);
    }
}