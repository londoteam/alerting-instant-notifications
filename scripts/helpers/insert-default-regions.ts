import { getDatabase } from '../config';
import { logger } from '../../src/winston';
import { Db } from 'mongodb';


export async function insertDefaultRegions(): Promise<any> {

    const regions = [
        {
            code: 'DRC',
            label: 'CENTRALES',
            email_dr: 'Omer.NTERE@bicec.com',
            email_dr_adj_1: 'Josue.Mboundje@bicec.com',
            email_dr_adj_2: 'Paul.TIEDJOP@bicec.com',

        },
        {
            code: 'DRM',
            label: 'MARITIMES',
            email_dr: 'Joseph-Rene.BITEKE@bicec.com',
            email_dr_adj_1: 'Didymus.NGUSANTUNG@bicec.com',
        },
        {
            code: 'SIEGE',
            label: 'SIEGE',
            email_dr: 'ComiteExecutif@bicec.com',
        }
    ]

    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('regions').insertMany(regions);
        logger.info(`${result.n} documents inserted in 'regions' collection`);
    } catch (error) {
        logger.error(`default regions insertion failed \n${error.stack}`);
    }
}