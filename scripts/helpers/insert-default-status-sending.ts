import { logger } from '../../src/winston';
import { getDatabase } from '../config';
import { Db } from 'mongodb';

export async function insertDefaultStatusSending(): Promise<any> {

    const statusSending = [
        {
            format: 100,
            sending: false
        },
        {
            format: 200,
            sending: false
        }
    ]

    const db: Db = await getDatabase();

    try {
        const data = await db.collection('notifications').insertMany(statusSending);
        logger.info(`${data.result.n} documents "statusSending" inserted in 'notifications' collection`);
    } catch (error) {
        logger.error(`default "statusSending" insertion failed \n${error.stack}`);
    }
}