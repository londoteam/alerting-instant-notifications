import { getDatabase } from '../config';
import { logger } from '../../src/winston';
import { Db } from 'mongodb';


export async function insertDefaultemplatesAlias(): Promise<any> {

    const alias = [
        {
            option: 2,
            alias: 'DEB',
            ope: '005'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '007'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '008'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '010'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '016'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '020'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '022'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '024'
        },
        {
            option: 2,
            alias: '',
            ope: '025'
        },
        {
            option: 2,
            alias: '',
            ope: '026'
        },
        {
            option: 2,
            alias: '',
            ope: '055'
        },
        {
            option: 2,
            alias: '',
            ope: '057'
        },
        {
            option: 2,
            alias: '',
            ope: '078'
        },
        {
            option: 2,
            alias: '',
            ope: '085'
        },
        {
            option: 2,
            alias: '',
            ope: '090'
        },
        {
            option: 2,
            alias: '',
            ope: '093'
        },
        {
            option: 2,
            alias: '',
            ope: '099'
        },
        {
            option: 2,
            alias: '',
            ope: '111'
        },
        {
            option: 2,
            alias: '',
            ope: '114'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '124'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '125'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '126'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '127'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '128'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '129'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '130'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '131'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '132'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '133'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '134'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '135'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '136'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '137'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '138'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '139'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '140'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '141'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '142'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '143'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '144'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '145'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '146'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '147'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '148'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '149'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '150'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '151'
        },
        {
            option: 2,
            alias: '',
            ope: '167'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '168'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '169'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '170'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '171'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '173'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '174'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '175'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '176'
        },
        {
            option: 2,
            alias: '',
            ope: '183'
        },
        {
            option: 2,
            alias: '',
            ope: '194'
        },
        {
            option: 2,
            alias: 'SAL',
            ope: '196'
        },
        {
            option: 2,
            alias: '',
            ope: '311'
        },
        {
            option: 2,
            alias: '',
            ope: '312'
        },
        {
            option: 2,
            alias: '',
            ope: '313'
        },
        {
            option: 2,
            alias: '',
            ope: '314'
        },
        {
            option: 2,
            alias: '',
            ope: '316'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '317'
        },
        {
            option: 2,
            alias: 'CRE',
            ope: '318'
        },
        {
            option: 2,
            alias: '',
            ope: '442'
        },
        {
            option: 2,
            alias: '',
            ope: '443'
        },
        {
            option: 2,
            alias: '',
            ope: '444'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '531'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '568'
        },
        {
            option: 2,
            alias: 'DEB',
            ope: '574'
        },

        // OPTION 1 Code
        {
            option: 1,
            alias: 'DEB',
            ope: '124'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '125'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '126'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '127'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '128'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '129'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '130'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '131'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '132'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '133'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '134'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '135'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '136'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '137'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '138'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '139'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '140'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '141'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '142'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '143'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '144'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '145'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '146'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '147'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '148'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '149'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '150'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '151'
        },
        {
            option: 1,
            alias: '',
            ope: '167'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '168'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '169'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '170'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '171'
        },
        {
            option: 1,
            alias: 'DEB',
            ope: '173'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '174'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '175'
        },
        {
            option: 1,
            alias: 'CRE',
            ope: '176'
        },
        {
            option: 1,
            alias: '',
            ope: '183'
        },
        {
            option: 1,
            alias: '',
            ope: '194'
        },
        {
            option: 1,
            alias: 'SAL',
            ope: '196'
        },
    ]

    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('templates-alias').insertMany(alias);
        logger.info(`${result.n} documents inserted in 'templates-alias' collection`);
    } catch (error) {
        logger.error(`default templates-alias insertion failed \n${error.stack}`);
    }
}