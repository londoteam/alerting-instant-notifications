import { logger } from '../../src/winston';
import { getDatabase } from '../config';
import * as moment from 'moment';
import { Db } from 'mongodb';

export async function insertDefaultTemplatesOperationsCartAndCheckbook(): Promise<any> {

    const db: Db = await getDatabase();

    const availableCheckbook = `Cher(e) {{NOMLIB}} {{NOM}}, votre chéquier est disponible en agence. BCI SANGO`;
    const availableCheckbookEng = `Dear {{NOMLIB}} {{NOM}}, your checkbook is available at your branch. BCI SANGO`;
    const availableCart = `Cher(e) {{NOMLIB}} {{NOM}}, votre carte BCI est à votre agence. Merci de la retirer au plus tot. BCI SANGO`;
    const availableCartEng = `Dear {{NOMLIB}} {{NOM}}, your BCI card is at your branch. Please collect it as soon as possible. BCI SANGO`;
    const availableCartMore30Days = `Cher(e) {{NOMLIB}} {{NOM}}, votre carte BCI est à votre agence depuis plus de 30 jours. Merci de la retirer au plus tot. BCI SANGO`;
    const availableCartMore30DaysEng = `Dear {{NOMLIB}} {{NOM}}, your BCI card has been at your branch for over 30 days. Please collect it as soon as possible. BCI SANGO`;
    const availableCartMore60Days = `Cher(e) {{NOMLIB}} {{NOM}}, votre carte BCI est à votre agence depuis plus de 60 jours. Merci de la retirer sans tarder sous peine de destruction`;
    const availableCartMore60DaysEng = `Dear {{NOMLIB}} {{NOM}}, your BCI card has been at your branch for over 60 days. Please remove it immediately or it will be destroyed`;
    const variables = [
        { name: 'AGE', type: 'STRING', desc: `CODE AGENCE` },
        { name: 'NCP', type: 'STRING', desc: `NUMERO DE COMPTE` },
        { name: 'EVE', type: 'STRING', desc: `NUMERO DE DEMANDE` },
        { name: 'DAP', type: 'STRING', desc: `DATE DE PERSONNALISATION` },
        { name: 'SIT', type: 'STRING', desc: `SITUATION DE LA DEMANDE ("D" = Demandée, "P" = Personnalisée, "R" = Chéquiers remis au client, "S" = Chéquiers détruits)` },
        { name: 'NOMLIB', type: 'STRING', desc: `NOMLIB` },
        { name: 'NOM', type: 'STRING', desc: `NOM` },
        { name: 'NUM_TEL', type: 'STRING', desc: `NUM_TEL` },
    ];
    const templates = [
        {
            desc: `Notification envoyée pour : Chéquiers disponibles en agences`,
            label: `Chéquiers disponibles en agences`,
            subject: `[BCI SANGO] Chéquiers disponibles en agences`,
            operationCode: 'CHECKBOOK',
            sms: {
                frenchText: availableCheckbook, englishText: availableCheckbookEng
            },
            email: {
                frenchText: '', englishText: ''
            },
            variables,
            priority: 3,
            notifyDate: ['J+0', 'J+7', 'J+14'],
            dates: { 'created': moment().subtract(1, 'week').valueOf(), 'updated': moment().valueOf() },
            enabled: true
        },
        {
            desc: `Notification envoyée pour : Cartes disponibles en agences`,
            label: `Cartes disponibles en agences`,
            subject: `[BCI SANGO] Cartes disponibles en agences`,
            operationCode: 'CART',
            sms: {
                frenchText: availableCart, englishText: availableCartEng
            },
            email: {
                frenchText: '', englishText: ''
            },
            variables: [
                { name: 'AGE', type: 'STRING', desc: `CODE AGENCE` },
                { name: 'NCP', type: 'STRING', desc: `NUMERO DE COMPTE` },
                { name: 'CLI', type: 'STRING', desc: `CODE CLIENT` },
                { name: 'DSIT', type: 'STRING', desc: `DSIT` },
                { name: 'NOMLIB', type: 'STRING', desc: `NOMLIB` },
                { name: 'NOM', type: 'STRING', desc: `NOM` },
                { name: 'NUM_TEL', type: 'STRING', desc: `NUM_TEL` },
            ],
            priority: 3,
            notifyDate: ['J+0', 'J+7', 'J+14'],
            dates: { 'created': moment().subtract(1, 'week').valueOf(), 'updated': moment().valueOf() },
            enabled: true
        },
        {
            desc: `Notification envoyée pour : Cartes disponibles en agences depuis plus de 30 Jours`,
            label: `Cartes disponibles en agences depuis plus de 30 Jours`,
            subject: `[BCI SANGO] Cartes disponibles en agences depuis plus de 30 Jours`,
            operationCode: 'CART_30',
            sms: {
                frenchText: availableCartMore30Days, englishText: availableCartMore30DaysEng
            },
            email: {
                frenchText: '', englishText: ''
            },
            variables: [
                { name: 'AGE', type: 'STRING', desc: `CODE AGENCE` },
                { name: 'NCP', type: 'STRING', desc: `NUMERO DE COMPTE` },
                { name: 'CLI', type: 'STRING', desc: `CODE CLIENT` },
                { name: 'DSIT', type: 'STRING', desc: `DSIT` },
                { name: 'NOMLIB', type: 'STRING', desc: `NOMLIB` },
                { name: 'NOM', type: 'STRING', desc: `NOM` },
                { name: 'NUM_TEL', type: 'STRING', desc: `NUM_TEL` },
            ],
            priority: 3,
            notifyDate: ['J+0', 'J+7', 'J+14'],
            dates: { 'created': moment().subtract(1, 'week').valueOf(), 'updated': moment().valueOf() },
            enabled: true
        },
        {
            desc: `Notification envoyée pour : Cartes disponibles en agences depuis plus de 60 Jours`,
            label: `Cartes disponibles en agences depuis plus de 60 Jours`,
            subject: `[BCI SANGO] Cartes disponibles en agences depuis plus de 60 Jours`,
            operationCode: 'CART_60',
            sms: {
                frenchText: availableCartMore60Days, englishText: availableCartMore60DaysEng
            },
            email: {
                frenchText: '', englishText: ''
            },
            variables: [
                { name: 'AGE', type: 'STRING', desc: `CODE AGENCE` },
                { name: 'NCP', type: 'STRING', desc: `NUMERO DE COMPTE` },
                { name: 'CLI', type: 'STRING', desc: `CODE CLIENT` },
                { name: 'DSIT', type: 'STRING', desc: `DSIT` },
                { name: 'NOMLIB', type: 'STRING', desc: `NOMLIB` },
                { name: 'NOM', type: 'STRING', desc: `NOM` },
                { name: 'NUM_TEL', type: 'STRING', desc: `NUM_TEL` },
            ],
            priority: 3,
            notifyDate: ['J+0', 'J+7', 'J+14'],
            dates: { 'created': moment().subtract(1, 'week').valueOf(), 'updated': moment().valueOf() },
            enabled: true
        },
    ];

    try {
        const { result } = await db.collection('templates_operations').insertMany(templates);
        logger.info(`${result.n} documents inserted in 'templates_operations' collection`);
    } catch (error) {
        logger.error(`Default templates_operations insertion failed \n${error.stack}`);
    }
}