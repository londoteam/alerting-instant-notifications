import { getDatabase } from '../config';
import { logger } from '../../src/winston';
import { config } from '../../src/config';


export async function dropDatabase(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop database`); }

    try {
        const db = await getDatabase();
        await db.dropDatabase();
        logger.info(`database '${config.get('db.name')}' dropped successfully`);
    } catch (error) {
        logger.error(`dropping '${config.get('db.name')}' failed`);
        process.exit(1);
    };

};

export async function dropTemplateCredit(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop templates_credits collection`); }

    try {
        const db = await getDatabase();
        await db.collection('templates_credits').deleteMany({});
        logger.info(`collection templates_credits dropped successfully`);
    } catch (error) {
        logger.error(`dropping collection templates_credits failed`);
        process.exit(1);
    };
};

export async function dropTemplateOperations(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop templates_operations collection`); }

    try {
        const db = await getDatabase();
        await db.collection('templates_operations').deleteMany({});
        logger.info(`collection templates_operations dropped successfully`);
    } catch (error) {
        logger.error(`dropping collection templates_operations failed`);
        process.exit(1);
    };
};

export async function dropQueries(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop queries collection`); }

    try {
        const db = await getDatabase();
        await db.collection('queries').drop();
        logger.info(`collection queries dropped successfully`);
    } catch (error) {
        logger.error(`dropping collection queries failed`);
        process.exit(1);
    };
};

export async function dropSetting(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop settings collection`); }

    try {
        const db = await getDatabase();
        await db.collection('settings').drop();
        logger.info(`collection settings dropped successfully`);
    } catch (error) {
        logger.error(`dropping collection settings failed`);
        process.exit(1);
    };
};

export async function dropOperationsByProductCode(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop operations_codes collection`); }

    try {
        const db = await getDatabase();
        await db.collection('operations_codes').deleteMany({});
        logger.info(`collection operations_codes dropped successfully`);
    } catch (error) {
        logger.error(`dropping collection operations_codes failed`);
        process.exit(1);
    };
};

export async function dropTfj(reset: boolean = false): Promise<any> {

    if (!reset) { return logger.info(`skip drop tfj collection`); }

    try {
        const db = await getDatabase();
        await db.collection('tfj').deleteMany({});
        logger.info(`collection tfj dropped successfully`);
    } catch (error) {
        logger.error(`dropping collection tfj failed`);
        process.exit(1);
    };
};