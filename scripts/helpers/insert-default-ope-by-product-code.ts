import { getDatabase } from '../config';
import { logger } from '../../src/winston';
import { Db } from 'mongodb';


export async function insertDefaultOperationCodeByProductCode(): Promise<any> {

    const data = [
        {
            cprod: '245', // OPTION 1
            operations: ['124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '136', '137', '138', '139', '140', '141', '142', '143', '144', '145', '146', '147', '148', '149', '150', '151', '167', '168', '169', '170', '171', '173', '174', '175', '176', '183', '194', '196', '596'],
        },
        {
            cprod: '246', // OPTION 2
            operations: ['005', '007', '008', '010', '016', '020', '022', '024', '025', '026', '055', '057', '078', '085', '090', '093', '099', '111', '114', '124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '136', '137', '138', '139', '140', '141', '142', '143', '144', '145', '146', '147', '148', '149', '150', '151', '167', '168', '169', '170', '171', '173', '174', '175', '176', '183', '194', '196', '311', '312', '313', '314', '316', '317', '318', '442', '443', '444', '531', '568', '574', '596'],
        }
    ];

    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('operations_codes').insertMany(data);
        logger.info(`${result.n} documents inserted in 'operations_codes' collection`);
    } catch (error) {
        logger.error(`default operations-code insertion failed \n${error.stack}`);
    }
}