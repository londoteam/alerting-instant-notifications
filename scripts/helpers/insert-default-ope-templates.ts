import { logger } from '../../src/winston';
import { getDatabase } from '../config';
import { readFile, utils } from 'xlsx';
import * as moment from 'moment';
import { Db } from 'mongodb';
import { get } from 'lodash';

export async function insertDefaultTemplatesOperations(): Promise<any> {

    const db: Db = await getDatabase();

    const smsD = `Bonjour, nous vous informons du DEBIT de {{MONTANT}} XAF sur votre compte {{NCP1}} le {{DATE_EVENEMENT}} à {{TIME_EVENEMENT}}, solde actuel {{SOLDE_COMPTE1}} XAF. BCI SANGO`
    const smsC = `Bonjour, nous vous informons du CREDIT de {{MONTANT}} XAF sur votre compte {{NCP2}} le {{DATE_EVENEMENT}} à {{TIME_EVENEMENT}}, solde actuel {{SOLDE_COMPTE2}} XAF. BCI SANGO`
    const smsMS = `Bonjour, les dernieres operations sur votre compte {{NCP}} : {{MONTANT_1}} le {{DATE_1}}, {{MONTANT_2}} le {{DATE_2}}, {{MONTANT_3}} le {{DATE_3}}, solde {{SOLDE_COMPTE}}`

    const emailD =
    `Chèr(e) {{CIVILITE1}} {{NOM1}},

    Nous vous informons du DEBIT de {{MONTANT}} XAF sur votre compte {{NCP1}} le {{DATE_EVENEMENT}} à {{TIME_EVENEMENT}}, solde actuel {{SOLDE_COMPTE1}} XAF.

    Bonne Journee BCI SANGO`;
    const emailC =
    `{{CIVILITE2}} {{NOM2}},

    Nous vous informons du CREDIT de {{MONTANT}} XAF sur votre compte {{NCP2}} le {{DATE_EVENEMENT}} à {{TIME_EVENEMENT}}, solde actuel {{SOLDE_COMPTE2}} XAF.

    Bonne Journee BCI SANGO`;
    const emailMS =
    `Bonjour {{CIVILITE}} {{NOM_CLIENT}},

    Votre relevé électronique ci-joint est prêt à être affiché en format PDF. Vous pouvez le sauvegarder, l'afficher  et l'imprimer à votre guise. 
    Pour garantir la sécurité et la confidentialité, une protection par mot de passe à été intégrée au document joint.
    Lorsque  vous serez invité(e) à entrer le mot de passe , veuillez entrer votre numéro de compte complet {{NCP}} comme indiqué ci-dessous.

    Exemple:
    Numéro de compte: {{NCP}}
    Mot de passe: numéro de compte complet

    Bonne Journee BCI SANGO`;

    const smsEngD = `Hello, we hereby inform you of the DEBIT of {{MONTANT}} XAF on your {{NCP1}} account on {{DATE_EVENEMENT}} at {{TIME_EVENEMENT}}, current balance {{SOLDE_COMPTE1}} XAF. BCI SANGO`
    const smsEngC = `Hello, we hereby inform you of the CREDIT of {{MONTANT}} XAF on your account {{NCP2}} on {{DATE_EVENEMENT}} at {{TIME_EVENEMENT}}, current balance {{SOLDE_COMPTE2}} XAF. BCI SANGO`
    const smsEngMS = `Hello, the last operations on your account {{NCP}}: {{MONTANT_1}} on {{DATE_1}}, {{MONTANT_2}} on {{DATE_2}}, {{MONTANT_3}} on {{DATE_3}}, balance {{SOLDE_COMPTE}}`;

    const emailEngD =
    `Dear {{CIVILITE1}} {{NOM1}},

    We are informing you of the DEBIT of {{MONTANT}} XAF on your {{NCP1}} account on {{DATE_EVENEMENT}} at {{TIME_EVENEMENT}}, current balance {{SOLDE_COMPTE1}} XAF.

    Have a nice day BCI SANGO`;
    const emailEngC =
    `{{CIVILITE2}} {{NOM2}},

    We inform you of the CREDIT of {{MONTANT}} XAF on your account {{NCP2}} on {{DATE_EVENEMENT}} at {{TIME_EVENEMENT}}, current balance {{SOLDE_COMPTE2}} XAF.

    Have a nice day BCI SANGO`;
    const emailEngMS =
    `Hello {{CIVILITE}} {{NOM_CLIENT}},

    Your attached electronic statement is ready to be viewed in PDF format. You can save it, view it and print it as you wish.
    To ensure security and confidentiality, password protection has been incorporated into the attached document.
    When prompted for the password, please enter your full account number {{NCP}} as shown below.

    Example:
    Account number: {{NCP}} 
    Password: full account number

    Have a nice day BCI SANGO`;


    try {

        const workbook = readFile(`${__dirname}/data/BCI_SANGO_2_14042021.xlsx`,);
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];

        const variables = [
            { name: 'AGSA', type: 'STRING', desc: `AGSA` },
            { name: 'AGE', type: 'STRING', desc: `CODE AGENCE` },
            { name: 'OPE', type: 'STRING', desc: `CODE OPERATION` },
            { name: 'EVE', type: 'STRING', desc: `EVE` },
            { name: 'TYP', type: 'STRING', desc: `TYP` },
            { name: 'NDOS', type: 'STRING', desc: `NDOS` },
            { name: 'AGE1', type: 'STRING', desc: `AGE COMPTE` },
            { name: 'DEV1', type: 'STRING', desc: `DEV1` },
            { name: 'NCP1', type: 'STRING', desc: `NCP1` },
            { name: 'SUF1', type: 'STRING', desc: `SUF1` },
            { name: 'CLC1', type: 'STRING', desc: `CLC DEBIT` },
            { name: 'CLI1', type: 'STRING', desc: `CODE CLIENT DEBIT` },
            { name: 'NOM1', type: 'STRING', desc: `NOM DEBIT` },
            { name: 'GES1', type: 'STRING', desc: `GES1` },
            { name: 'SEN1', type: 'STRING', desc: `SEN1` },
            { name: 'MHT1', type: 'STRING', desc: `MHT1` },
            { name: 'MON1', type: 'STRING', desc: `MON1` },
            { name: 'DVA1', type: 'STRING', desc: `DVA1` },
            { name: 'EXO1', type: 'STRING', desc: `EXO1` },
            { name: 'SOL1', type: 'STRING', desc: `SOLDE COMPTE DEBIT` },
            { name: 'INDH1', type: 'STRING', desc: `INDH1` },
            { name: 'INDS1', type: 'STRING', desc: `INDS1` },
            { name: 'DESA1', type: 'STRING', desc: `DESA1` },
            { name: 'DESA2', type: 'STRING', desc: `DESA2` },
            { name: 'DESA3', type: 'STRING', desc: `DESA3` },
            { name: 'DESA4', type: 'STRING', desc: `DESA4` },
            { name: 'DESA5', type: 'STRING', desc: `DESA5` },
            { name: 'AGE2', type: 'STRING', desc: `AGE2` },
            { name: 'DEV2', type: 'STRING', desc: `DEV2` },
            { name: 'NCP2', type: 'STRING', desc: `NCP2` },
            { name: 'SUF2', type: 'STRING', desc: `SUF2` },
            { name: 'CLC2', type: 'STRING', desc: `CLC2` },
            { name: 'CLI2', type: 'STRING', desc: `CODE CLIENT CREDIT` },
            { name: 'NOM2', type: 'STRING', desc: `NOM2` },
            { name: 'GES2', type: 'STRING', desc: `GES2` },
            { name: 'SEN2', type: 'STRING', desc: `SEN2` },
            { name: 'MHT2', type: 'STRING', desc: `MHT2` },
            { name: 'MON2', type: 'STRING', desc: `MON2` },
            { name: 'DVA2', type: 'STRING', desc: `DVA2` },
            { name: 'DIN', type: 'STRING', desc: `DIN` },
            { name: 'EXO2', type: 'STRING', desc: `EXO2` },
            { name: 'SOL2', type: 'STRING', desc: `SOLDE COMPTE CREDIT` },
            { name: 'INDH2', type: 'STRING', desc: `INDH2` },
            { name: 'INDS2', type: 'STRING', desc: `INDS2` },
            { name: 'DESC1', type: 'STRING', desc: `DESC1` },
            { name: 'DESC2', type: 'STRING', desc: `DESC2` },
            { name: 'DESC3', type: 'STRING', desc: `DESC3` },
            { name: 'DESC4', type: 'STRING', desc: `DESC4` },
            { name: 'DESC5', type: 'STRING', desc: `DESC5` },
            { name: 'ETAB', type: 'STRING', desc: `ETAB` },
            { name: 'GUIB', type: 'STRING', desc: `GUIB` },
            { name: 'NOME', type: 'STRING', desc: `NOME` },
            { name: 'DOMI', type: 'STRING', desc: `DOMI` },
            { name: 'ADB1', type: 'STRING', desc: `ADB1` },
            { name: 'ADB2', type: 'STRING', desc: `ADB2` },
            { name: 'ADB3', type: 'STRING', desc: `ADB3` },
            { name: 'VILB', type: 'STRING', desc: `VILB` },
            { name: 'CPOB', type: 'STRING', desc: `CPOB` },
            { name: 'CPAY', type: 'STRING', desc: `CPAY` },
            { name: 'ETABR', type: 'STRING', desc: `ETABR` },
            { name: 'GUIBR', type: 'STRING', desc: `GUIBR` },
            { name: 'COMB', type: 'STRING', desc: `COMB` },
            { name: 'CLEB', type: 'STRING', desc: `CLEB` },
            { name: 'NOMB', type: 'STRING', desc: `NOMB` },
            { name: 'MBAN', type: 'STRING', desc: `MBAN` },
            { name: 'DVAB', type: 'STRING', desc: `DVAB` },
            { name: 'CAI1', type: 'STRING', desc: `CAI1` },
            { name: 'TYC1', type: 'STRING', desc: `TYC1` },
            { name: 'DCAI1', type: 'STRING', desc: `DCAI1` },
            { name: 'SCAI1', type: 'STRING', desc: `SCAI1` },
            { name: 'MCAI1', type: 'STRING', desc: `MCAI1` },
            { name: 'ARRC1', type: 'STRING', desc: `ARRC1` },
            { name: 'CAI2', type: 'STRING', desc: `CAI2` },
            { name: 'TYC2', type: 'STRING', desc: `TYC2` },
            { name: 'DCAI2', type: 'STRING', desc: `DCAI2` },
            { name: 'SCAI2', type: 'STRING', desc: `SCAI2` },
            { name: 'MCAI2', type: 'STRING', desc: `MCAI2` },
            { name: 'ARRC2', type: 'STRING', desc: `ARRC2` },
            { name: 'DEV', type: 'STRING', desc: `DEV` },
            { name: 'MHT', type: 'STRING', desc: `MHT` },
            { name: 'MNAT', type: 'STRING', desc: `MNAT` },
            { name: 'MBOR', type: 'STRING', desc: `MBOR` },
            { name: 'NBOR', type: 'STRING', desc: `NBOR` },
            { name: 'NBLIG', type: 'STRING', desc: `NBLIG` },
            { name: 'TCAI2', type: 'STRING', desc: `TCAI2` },
            { name: 'TCAI3', type: 'STRING', desc: `TCAI3` },
            { name: 'NAT', type: 'STRING', desc: `NAT` },
            { name: 'NATO', type: 'STRING', desc: `NATO` },
            { name: 'OPEO', type: 'STRING', desc: `OPEO` },
            { name: 'EVEO', type: 'STRING', desc: `EVEO` },
            { name: 'PIEO', type: 'STRING', desc: `PIEO` },
            { name: 'DOU', type: 'STRING', desc: `DOU` },
            { name: 'DCO', type: 'STRING', desc: `DCO` },
            { name: 'ETA', type: 'STRING', desc: `ETA` },
            { name: 'ETAP', type: 'STRING', desc: `ETAP` },
            { name: 'NBV', type: 'STRING', desc: `NBV` },
            { name: 'NVAL', type: 'STRING', desc: `NVAL` },
            { name: 'UTI', type: 'STRING', desc: `UTI` },
            { name: 'UTF', type: 'STRING', desc: `UTF` },
            { name: 'UTA', type: 'STRING', desc: `UTA` },
            { name: 'MOA', type: 'STRING', desc: `MOA` },
            { name: 'MOF', type: 'STRING', desc: `MOF` },
            { name: 'LIB1', type: 'STRING', desc: `LIB1` },
            { name: 'LIB2', type: 'STRING', desc: `LIB2` },
            { name: 'LIB3', type: 'STRING', desc: `LIB3` },
            { name: 'LIB4', type: 'STRING', desc: `LIB4` },
            { name: 'LIB5', type: 'STRING', desc: `LIB5` },
            { name: 'LIB6', type: 'STRING', desc: `LIB6` },
            { name: 'LIB7', type: 'STRING', desc: `LIB7` },
            { name: 'LIB8', type: 'STRING', desc: `LIB8` },
            { name: 'LIB9', type: 'STRING', desc: `LIB9` },
            { name: 'LIB10', type: 'STRING', desc: `LIB10` },
            { name: 'AGEC', type: 'STRING', desc: `AGEC` },
            { name: 'AGEP', type: 'STRING', desc: `AGEP` },
            { name: 'INTR', type: 'STRING', desc: `INTR` },
            { name: 'ORIG', type: 'STRING', desc: `ORIG` },
            { name: 'RLET', type: 'STRING', desc: `RLET` },
            { name: 'CATR', type: 'STRING', desc: `CATR` },
            { name: 'CEB', type: 'STRING', desc: `CEB` },
            { name: 'PLB', type: 'STRING', desc: `PLB` },
            { name: 'CCO', type: 'STRING', desc: `CCO` },
            { name: 'DRET', type: 'STRING', desc: `DRET` },
            { name: 'NATP', type: 'STRING', desc: `NATP` },
            { name: 'NUMP', type: 'STRING', desc: `NUMP` },
            { name: 'DATP', type: 'STRING', desc: `DATP` },
            { name: 'NOMP', type: 'STRING', desc: `NOMP` },
            { name: 'AD1P', type: 'STRING', desc: `AD1P` },
            { name: 'AD2P', type: 'STRING', desc: `AD2P` },
            { name: 'DELP', type: 'STRING', desc: `DELP` },
            { name: 'SERIE', type: 'STRING', desc: `SERIE` },
            { name: 'NCHE', type: 'STRING', desc: `NCHE` },
            { name: 'CHQL', type: 'STRING', desc: `CHQL` },
            { name: 'CHQC', type: 'STRING', desc: `CHQC` },
            { name: 'CAB', type: 'STRING', desc: `CAB` },
            { name: 'NCFF', type: 'STRING', desc: `NCFF` },
            { name: 'CSA', type: 'STRING', desc: `CSA` },
            { name: 'CFRA', type: 'STRING', desc: `CFRA` },
            { name: 'NEFF', type: 'STRING', desc: `NEFF` },
            { name: 'TEFF', type: 'STRING', desc: `TEFF` },
            { name: 'DECH', type: 'STRING', desc: `DECH` },
            { name: 'TIRE', type: 'STRING', desc: `TIRE` },
            { name: 'AGTI', type: 'STRING', desc: `AGTI` },
            { name: 'AGRE', type: 'STRING', desc: `AGRE` },
            { name: 'NBJI', type: 'STRING', desc: `NBJI` },
            { name: 'PTFC', type: 'STRING', desc: `PTFC` },
            { name: 'EFAV', type: 'STRING', desc: `EFAV` },
            { name: 'NAVL', type: 'STRING', desc: `NAVL` },
            { name: 'EDOM', type: 'STRING', desc: `EDOM` },
            { name: 'EOPP', type: 'STRING', desc: `EOPP` },
            { name: 'EFAC', type: 'STRING', desc: `EFAC` },
            { name: 'MOTI', type: 'STRING', desc: `MOTI` },
            { name: 'ENVACC', type: 'STRING', desc: `ENVACC` },
            { name: 'ENOM', type: 'STRING', desc: `ENOM` },
            { name: 'VICL', type: 'STRING', desc: `VICL` },
            { name: 'TECO', type: 'STRING', desc: `TECO` },
            { name: 'TENV', type: 'STRING', desc: `TENV` },
            { name: 'EXJO', type: 'STRING', desc: `EXJO` },
            { name: 'ORG', type: 'STRING', desc: `ORG` },
            { name: 'CAI3', type: 'STRING', desc: `CAI3` },
            { name: 'MCAI3', type: 'STRING', desc: `MCAI3` },
            { name: 'FORC', type: 'STRING', desc: `FORC` },
            { name: 'OCAI3', type: 'STRING', desc: `OCAI3` },
            { name: 'NCAI3', type: 'STRING', desc: `NCAI3` },
            { name: 'CSP1', type: 'STRING', desc: `CSP1` },
            { name: 'CSP2', type: 'STRING', desc: `CSP2` },
            { name: 'CSP3', type: 'STRING', desc: `CSP3` },
            { name: 'CSP4', type: 'STRING', desc: `CSP4` },
            { name: 'CSP5', type: 'STRING', desc: `CSP5` },
            { name: 'NDOM', type: 'STRING', desc: `NDOM` },
            { name: 'CMOD', type: 'STRING', desc: `CMOD` },
            { name: 'DEVF', type: 'STRING', desc: `DEVF` },
            { name: 'NCPF', type: 'STRING', desc: `NCPF` },
            { name: 'SUFF', type: 'STRING', desc: `SUFF` },
            { name: 'MONF', type: 'STRING', desc: `MONF` },
            { name: 'DVAF', type: 'STRING', desc: `DVAF` },
            { name: 'EXOF', type: 'STRING', desc: `EXOF` },
            { name: 'AGEE', type: 'STRING', desc: `AGEE` },
            { name: 'DEVE', type: 'STRING', desc: `DEVE` },
            { name: 'NCPE', type: 'STRING', desc: `NCPE` },
            { name: 'SUFE', type: 'STRING', desc: `SUFE` },
            { name: 'CLCE', type: 'STRING', desc: `CLCE` },
            { name: 'NCPI', type: 'STRING', desc: `NCPI` },
            { name: 'SUFI', type: 'STRING', desc: `SUFI` },
            { name: 'MIMP', type: 'STRING', desc: `MIMP` },
            { name: 'DVAI', type: 'STRING', desc: `DVAI` },
            { name: 'NCPP', type: 'STRING', desc: `NCPP` },
            { name: 'SUFP', type: 'STRING', desc: `SUFP` },
            { name: 'PRGA', type: 'STRING', desc: `PRGA` },
            { name: 'MRGA', type: 'STRING', desc: `MRGA` },
            { name: 'TERM', type: 'STRING', desc: `TERM` },
            { name: 'TVAR', type: 'STRING', desc: `TVAR` },
            { name: 'INTP', type: 'STRING', desc: `INTP` },
            { name: 'CAP', type: 'STRING', desc: `CAP` },
            { name: 'PRLL', type: 'STRING', desc: `PRLL` },
            { name: 'ANO', type: 'STRING', desc: `ANO` },
            { name: 'ETAB1', type: 'STRING', desc: `ETAB1` },
            { name: 'GUIB1', type: 'STRING', desc: `GUIB1` },
            { name: 'COM1B', type: 'STRING', desc: `COM1B` },
            { name: 'ETAB2', type: 'STRING', desc: `ETAB2` },
            { name: 'GUIB2', type: 'STRING', desc: `GUIB2` },
            { name: 'COM2B', type: 'STRING', desc: `COM2B` },
            { name: 'TCOM1', type: 'STRING', desc: `TCOM1` },
            { name: 'MCOM1', type: 'STRING', desc: `MCOM1` },
            { name: 'TCOM2', type: 'STRING', desc: `TCOM2` },
            { name: 'MCOM2', type: 'STRING', desc: `MCOM2` },
            { name: 'TCOM3', type: 'STRING', desc: `TCOM3` },
            { name: 'MCOM3', type: 'STRING', desc: `MCOM3` },
            { name: 'FRAI1', type: 'STRING', desc: `FRAI1` },
            { name: 'FRAI2', type: 'STRING', desc: `FRAI2` },
            { name: 'FRAI3', type: 'STRING', desc: `FRAI3` },
            { name: 'TTAX1', type: 'STRING', desc: `TTAX1` },
            { name: 'MTAX1', type: 'STRING', desc: `MTAX1` },
            { name: 'TTAX2', type: 'STRING', desc: `TTAX2` },
            { name: 'MTAX2', type: 'STRING', desc: `MTAX2` },
            { name: 'TTAX3', type: 'STRING', desc: `TTAX3` },
            { name: 'MTAX3', type: 'STRING', desc: `MTAX3` },
            { name: 'MNT1', type: 'STRING', desc: `MNT1` },
            { name: 'MNT2', type: 'STRING', desc: `MNT2` },
            { name: 'MNT3', type: 'STRING', desc: `MNT3` },
            { name: 'MNT4', type: 'STRING', desc: `MNT4` },
            { name: 'MNT5', type: 'STRING', desc: `MNT5` },
            { name: 'TYC3', type: 'STRING', desc: `TYC3` },
            { name: 'DCAI3', type: 'STRING', desc: `DCAI3` },
            { name: 'SCAI3', type: 'STRING', desc: `SCAI3` },
            { name: 'ARRC3', type: 'STRING', desc: `ARRC3` },
            { name: 'MHTD', type: 'STRING', desc: `MHTD` },
            { name: 'TCAI4', type: 'STRING', desc: `TCAI4` },
            { name: 'TOPE', type: 'STRING', desc: `TOPE` },
            { name: 'IMG', type: 'STRING', desc: `IMG` },
            { name: 'DATE_EVENEMENT', type: 'STRING', desc: `DATE_EVENEMENT` },
            { name: 'TIME_EVENEMENT', type: 'STRING', desc: `TIME_EVENEMENT` },
            { name: 'PAYSP', type: 'STRING', desc: `PAYSP` },
            { name: 'PDELP', type: 'STRING', desc: `PDELP` },
            { name: 'MANDA', type: 'STRING', desc: `MANDA` },
            { name: 'REFDOS', type: 'STRING', desc: `REFDOS` },
            { name: 'TCHFR', type: 'STRING', desc: `TCHFR` },
            { name: 'NIDNP', type: 'STRING', desc: `NIDNP` },
            { name: 'FRAISDIFF1', type: 'STRING', desc: `FRAISDIFF1` },
            { name: 'FRAISDIFF2', type: 'STRING', desc: `FRAISDIFF2` },
            { name: 'RNUM', type: 'STRING', desc: `RNUM` },
            { name: 'CIVILITE1', type: 'STRING', desc: `CIVILITE DEBIT` },
            { name: 'CIVILITE2', type: 'STRING', desc: `CIVILITE CREDIT` },
            { name: 'SENS_COMPTE1', type: 'STRING', desc: `SENS COMPTE DEBIT` },
            { name: 'SENS_COMPTE2', type: 'STRING', desc: `SENS COMPTE CREDIT` },
            { name: 'CODE_AGENCE', type: 'STRING', desc: `CODE AGENCE` },
            { name: 'ID', type: 'STRING', desc: `ID` },
            { name: 'SOLDE_COMPTE1', type: 'STRING', desc: `SOLDE COMPTE DEBIT` },
            { name: 'SOLDE_COMPTE2', type: 'STRING', desc: `SOLDE COMPTE CREDIT` },
            { name: 'CODE_EVENEMENT', type: 'STRING', desc: `CODE EVENEMENT` },
            { name: 'CODE_OPERATION', type: 'STRING', desc: `CODE OPERATION` },
            { name: 'CODE_CLIENT', type: 'STRING', desc: `CODE CLIENT` },
            { name: 'LIBELLE_AGENCE', type: 'STRING', desc: `LIBELLE AGENCE` },
            { name: 'DATE_EVENEMENT', type: 'STRING', desc: `DATE EVENEMENT` },
            { name: 'TIME_EVENEMENT', type: 'STRING', desc: `TIME EVENEMENT` },
            { name: 'NCP1', type: 'STRING', desc: `NUMERO COMPTE DEBIT` },
            { name: 'NCP2', type: 'STRING', desc: `NUMERO COMPTE CREDIT` },
            { name: 'LIBELLE_OPERATION', type: 'STRING', desc: `LIBELLE OPERATION` },
            { name: 'MONTANT', type: 'STRING', desc: `MONTANT OPERATION` },
            { name: 'PHONE1', type: 'STRING', desc: `PHONE DEBIT` },
            { name: 'PHONE2', type: 'STRING', desc: `PHONE CREDIT` },
            { name: 'NOM1', type: 'STRING', desc: `TITULAIRE COMPTE DEBIT` },
            { name: 'NOM2', type: 'STRING', desc: `TITULAIRE COMPTE CREDIT` },
            { name: 'LANG1', type: 'STRING', desc: `LANGUE COMPTE DEBIT` },
            { name: 'LANG2', type: 'STRING', desc: `LANGUE COMPTE CREDIT` },
            { name: 'EXTRACT_TIME', type: 'STRING', desc: `EXTRACT TIME` },
        ];

        const operations = utils.sheet_to_json(worksheet, { raw: true });
        const templates = [];
        for (const operation of operations) {

            const template = {
                desc: `Notification envoyée pour l'opération :  ${get(operation, 'LIB')}`.trim(),
                label: `${get(operation, 'LIB')}`.trim(),
                subject: `[BCI SANGO] ${get(operation, 'LIB')}`.trim(),
                operationCode: `${get(operation, 'OPE')}`.trim(),
                sms: {
                    debit: { frenchText: smsD, englishText: smsEngD },
                    credit: { frenchText: smsC, englishText: smsEngC }
                },
                email: {
                    debit: { frenchText: emailD, englishText: emailEngD },
                    credit: { frenchText: emailC, englishText: emailEngC }
                },
                variables,
                priority: 1,
                dates: { 'created': moment().subtract(1, 'week').valueOf(), 'updated': moment().valueOf() },
                enabled: true
            };
            templates.push(template);
        }
        templates.push({
            desc: `Notification envoyée pour l'opération : MINI_STATEMENT`,
            label: `MINI_STATEMENT`,
            subject: `[BCI SANGO] MINI_STATEMENT`.trim(),
            operationCode: 'MINI_STATEMENT',
            sms: {
                frenchText: smsMS,
                englishText: smsEngMS
            },
            email: {
                frenchText: emailMS,
                englishText: emailEngMS
            },
            variables,
            priority: 2,
            dates: { 'created': moment().subtract(1, 'week').valueOf(), 'updated': moment().valueOf() },
            enabled: true
        });

        const { result } = await db.collection('templates_operations').insertMany(templates);
        logger.info(`${result.n} documents inserted in 'templates_operations' collection`);
    } catch (error) {
        logger.error(`Default templates_operations insertion failed \n${error.stack}`);
    }
}