import { logger } from '../../src/winston';
import { getDatabase } from '../config';
import { Db } from 'mongodb';

export async function insertDefaultStatusinProcessSango(): Promise<any> {

    const statusInProcess = [
        {
            status: 100,
            inProcess: false
        },
        {
            status: 200,
            inProcess: false
        },
    ]

    const db: Db = await getDatabase();

    try {
        const data = await db.collection('sango_eve').insertMany(statusInProcess);
        logger.info(`${data.result.n} documents "statusInProcess" inserted in 'sango_eve' collection`);
    } catch (error) {
        logger.error(`default "statusInProcess" insertion failed \n${error.stack}`);
    }
}