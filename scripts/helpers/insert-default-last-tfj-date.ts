import { logger } from '../../src/winston';
import { getDatabase } from '../config';
import * as moment from 'moment';
import { Db } from 'mongodb';

export async function insertDefaultTfjUpdateDate(): Promise<any> {

    const lastTfjUpdate = {lastTfjDate: moment().valueOf()}
    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('tfj').insertOne(lastTfjUpdate);
        logger.info(`${result.n} documents inserted in 'tfj' collection`);
    } catch (error) {
        logger.error(`default tfj last update date insertion failed \n${error.stack}`);
    }
}