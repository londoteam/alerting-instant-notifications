import { logger } from '../../src/winston';
import { getDatabase } from '../config';
import { Db } from 'mongodb';

export async function insertDefaultSettings(): Promise<any> {

    const settings = [
        {
            cost: 7,
            type: 1,
            nbSimultaneousSending: 500,
            quota: 60000,
            bridge: 'PASSERELLE SMS LONDO TECHNOLOGY',
            enabled: true
        },
        {
            cost: 7,
            type: 2,
            nbSimultaneousSending: 800,
            quota: 70000,
            bridge: 'LONDO TECHNOLOGY',
            enabled: false
        },
        {
            cost: 0,
            type: 2,
            nbSimultaneousSending: 800,
            quota: 70000,
            bridge: 'BICEC',
            enabled: true
        }
    ];

    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('settings').insertMany(settings);
        logger.info(`${result.n} documents inserted in 'settings' collection`);
    } catch (error) {
        logger.error(`default settings insertion failed \n${error.stack}`);
    }
}