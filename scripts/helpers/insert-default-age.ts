import { Db } from 'mongodb';
import * as moment from 'moment';
import { read, utils } from 'xlsx'
import { getDatabase } from '../config';
import { logger } from '../../src/winston';


export async function insertDefaultAGE(): Promise<any> {

    const workbook = read(__dirname + "/data/agences.xlsx", { type: "file", cellDates: true });
    const firstSheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[firstSheetName];
    let age_list = utils.sheet_to_json(worksheet, { raw: true });

    age_list = age_list.map((age: any) => {
        const elmt = {
            AGE: age.AGE,
            LIBAGE: `${age.LIB}`.trim(),
            EMAIL_DA: `${age.EMAIL}`.toLowerCase(),
            CODEREG: age.CODE_REGION,
            LIBREG: age.LIBELLE_REGION,
            EMAIL_DR: `${age.EMAIL_DR}`.toLowerCase() || '',
            EMAIL_DR_ADJ_1: `${age.EMAIL_DR_ADJ_1}`.toLowerCase() || '',
            EMAIL_DR_ADJ_2: `${age.EMAIL_DR_ADJ_2}`.toLowerCase() || '',
            DCRE: moment().valueOf(),
            DMOD: moment().valueOf(),
            UTIL: ''
        }

        return elmt;
    })

    const db: Db = await getDatabase();

    try {
        const { result } = await db.collection('ages').insertMany(age_list);
        logger.info(`${result.n} documents inserted in 'ages' collection`);
    } catch (error) {
        logger.error(`default ages insertion failed \n${error.stack}`);
    }
}