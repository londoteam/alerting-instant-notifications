import { getDatabase } from '../config';
import { logger } from '../../src/winston';
import { Db } from 'mongodb';
import * as moment from 'moment';
import { get } from 'lodash';

export async function insertDefaultTemplatesCredit(): Promise<any> {

    const db: Db = await getDatabase();

    const variablesData = await db.collection('queries').findOne({});
    const variables = get(variablesData, 'variables', []);

    const templates = [

        // Relances clients réseau avant échéance
        {
            desc: 'Relance aux clients du réseau avant échéance',
            label: 'Notification avant échéance (clients réseau)',
            templateType: 1,
            subjectFR: `[ALERITNG BCI] Notification avant échéance`,
            subjectEN: `[ALERITNG BCI] Notification before deadline`,
            clientProfile: 200,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J-7',
            sms: {
                frenchText: `Cher(e) client BCI, veuillez réserver la somme de {{TOTAL_ECH}} FCFA sur votre compte afin d'honorer l'échéance du {{DATE_ECHEANCE}} de votre crédit {{NUM_DOSSIER}}-{{NUM_ECHEANCE}}.`,
                englishText: 'Dear BCI client, please reserve the sum of {{TOTAL_ECH}} FCFA in your account in order to honor the {{DATE_ECHEANCE}} due date of your {{NUM_DOSSIER}}-{{NUM_ECHEANCE}}.',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous vous prions de bien vouloir réserver la somme de {{TOTAL_ECH}} FCFA sur votre compte {{NUM_COMPTE_CLIENT}} afin d'honorer l'échéance de votre crédit due le {{DATE_ECHEANCE}}.

                Merci d'avoir choisi BCI CONGO.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We ask you to reserve the sum of {{TOTAL_ECH}} FCFA in your account {{NUM_COMPTE_CLIENT}} in order to honor the maturity of your credit due on {{DATE_ECHEANCE}}.

                Thank you for choosing BCI CONGO.`,
            },
            author: 'John Doe',
            enabled: true,
            priority: 3
        },

        // Relances clients corporate avant échéance
        {
            desc: 'Relance aux clients corporates avant échéance',
            label: 'Notification avant échéance (clients corporates)',
            templateType: 1,
            subjectFR: `[ALERITNG BCI] Notification avant échéance`,
            subjectEN: `[ALERITNG BCI] Notification before deadline`,
            clientProfile: 100,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J-7',
            sms: {
                frenchText: `Cher(e) client BCI, veuillez réserver la somme de {{TOTAL_ECH}} FCFA sur votre compte afin d'honorer l'échéance du {{DATE_ECHEANCE}} de votre crédit {{NUM_DOSSIER}}-{{NUM_ECHEANCE}}.`,
                englishText: 'Dear BCI client, please reserve the sum of {{TOTAL_ECH}} FCFA in your account in order to honor the {{DATE_ECHEANCE}} due date of your {{NUM_DOSSIER}}-{{NUM_ECHEANCE}}.',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous vous prions de bien vouloir réserver la somme de {{TOTAL_ECH}} FCFA sur votre compte {{NUM_COMPTE_CLIENT}} afin d'honorer l'échéance de votre crédit due le {{DATE_ECHEANCE}}.

                Merci d'avoir choisi BCI CONGO.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We ask you to reserve the sum of {{TOTAL_ECH}} FCFA in your account {{NUM_COMPTE_CLIENT}} in order to honor the maturity of your credit due on {{DATE_ECHEANCE}}.

                Thank you for choosing BCI CONGO.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },

        // Relances clients réseau si échéance impayée
        {
            desc: 'Notification aux clients du réseau pour non paiement échéance',
            label: 'Notification échéance impayée (clients réseau)',
            templateType: 2,
            subjectFR: `[ALERITNG BCI] Notification échéance impayée`,
            subjectEN: `[ALERITNG BCI] Notification of unpaid due date`,
            clientProfile: 200,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J+7',
            sms: {
                frenchText: `Cher(e) client(e), l'échéance du {{DATE_ECHEANCE}} de {{TOTAL_ECH}} XAF n'a pas été honorée. Votre impayé s'élève à {{MIMP}} XAF. Merci de vous rapprocher de votre agence.`,
                englishText: 'Dear customer, the {{DATE_ECHEANCE}} deadline of {{TOTAL_ECH}} XAF has not been honored. Your outstanding amount is {{MIMP}} XAF. Thank you for getting closer to your agency.',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous constatons avec regret que votre échéance du {{DATE_ECHEANCE}} de {{TOTAL_A_PAYER}} FCFA n'a pas été honorée.
                    Le montant total de vos impayés s'élève à ce jour à {{MIMP}} FCFA.
                    Nous vous invitons à régulariser votre situation.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We regret that your {{DATE_ECHEANCE}} deadline of {{TOTAL_A_PAYER}} FCFA has not been honored.
                    To date, the total amount of your unpaid bills amounts to {{MIMP}} FCFA.
                    We invite you to regularize your situation.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },

        // Relances clients corporate si échéance impayée
        {
            desc: 'Notification aux clients corporate pour non paiement échéance',
            label: 'Notification échéance impayée (clients corporates)',
            templateType: 2,
            subjectFR: `[ALERITNG BCI] Notification échéance impayée`,
            subjectEN: `[ALERITNG BCI] Notification of unpaid due date`,
            clientProfile: 100,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J+7',
            sms: {
                frenchText: `Cher(e) client(e), l'échéance du {{DATE_ECHEANCE}} de {{TOTAL_ECH}} XAF n'a pas été honorée. Votre impayé s'élève à {{MIMP}} XAF. Merci de vous rapprocher de votre agence.`,
                englishText: 'Dear customer, the {{DATE_ECHEANCE}} deadline of {{TOTAL_ECH}} XAF has not been honored. Your outstanding amount is {{MIMP}} XAF. Thank you for getting closer to your agency.',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous constatons avec regret que votre échéance du {{DATE_ECHEANCE}} de {{TOTAL_A_PAYER}} FCFA n'a pas été honorée.
                    Le montant total de vos impayés s'élève à ce jour à {{MIMP}} FCFA.
                    Nous vous invitons à régulariser votre situation.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We regret that your {{DATE_ECHEANCE}} deadline of {{TOTAL_A_PAYER}} FCFA has not been honored.
                    To date, the total amount of your unpaid bills amounts to {{MIMP}} FCFA.
                    We invite you to regularize your situation.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },


        // Première lettre de relance Client réseau
        {
            desc: 'Notification première lettre de relance clients réseau',
            label: 'Notification première lettre de relance (clients réseau)',
            templateType: 3,
            subjectFR: `[ALERTING BCI] Première lettre de relance`,
            subjectEN: `[ALERTING BCI] First letter of recovery`,
            clientProfile: 200,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J+14',
            sms: {
                frenchText: `{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, Le solde de votre compte N° {{NUM_COMPTE_CLIENT}} n'a pas permis le règlement de l'échéance au {{DATE_ECHEANCE}} de la somme de {{TOTAL_ECH}} FCFA de votre crédit {{NUM_DOSSIER}}. Le montant de ladite échéance a été classé dans la catégorie des impayés. Vous disposez désormais de 20 jours pour honorer vos engagements`,
                englishText: '{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, The balance of your account N° {{NUM_COMPTE_CLIENT}} did not allow the payment of the due date at {{DATE_ECHEANCE}} of the sum of {{TOTAL_ECH}} FCFA of your credit {{NUM_DOSSIER}}. The amount of the said due date has been classified in the category of arrears. You now have 20 days to honor your commitments',
            },
            email: {
                frenchText:
                    `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                        Nous constatons avec regret que le solde de votre compte n° {{NUM_COMPTE_CLIENT}} dans nos livres, n’a pas permis le règlement de l’échéance au {{DATE_ECHEANCE}}, de FCFA {{TOTAL_ECH}} du prêt de nominal FCFA {{MONTANT_PRET}}  qui vous avait été accordé par notre Etablissement.

                        Le montant de ladite échéance a été classé dans la catégorie des impayés, conformément à la règlementation en vigueur, dans l’attente de votre couverture dans un délai de vingt jours, à compte de l’envoi de la présente.

                    Dans cette attente, Veuillez agréer, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, l’expression de nos sentiments distingués.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We note with regret that the balance of your account n° {{NUM_COMPTE_CLIENT}} in our books, did not allow the payment of the due date at {{DATE_ECHEANCE}}, of FCFA {{TOTAL_ECH}} of the nominal loan FCFA {{MONTANT_PRET}} which had been granted to you by our Establishment.

                    The amount of the said due date has been classified in the category of unpaid bills, in accordance with the regulations in force, pending your coverage within twenty days. On account of the sending of this letter.

                In the meantime, Please accept, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, the expression of our distinguished feelings.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },

        // Première lettre de relance Client Corporate
        {
            desc: 'Notification première lettre de relance clients corporate',
            label: 'Notification première lettre de relance (clients corporate)',
            templateType: 3,
            subjectFR: `[ALERITNG BCI] Première lettre de relance`,
            subjectEN: `[ALERITNG BCI] First letter of recovery`,
            clientProfile: 100,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J+15',
            sms: {
                frenchText: `{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, Le solde de votre compte N° {{NUM_COMPTE_CLIENT}} n'a pas permis le règlement de l'échéance au {{DATE_ECHEANCE}} de la somme de {{TOTAL_ECH}} FCFA de votre crédit {{NUM_DOSSIER}}. Le montant de ladite échéance a été classé dans la catégorie des impayés. Vous disposez désormais de 20 jours pour honorer vos engagements`,
                englishText: '{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, The balance of your account N° {{NUM_COMPTE_CLIENT}} did not allow the payment of the due date at {{DATE_ECHEANCE}} of the sum of {{TOTAL_ECH}} FCFA of your credit {{NUM_DOSSIER}}. The amount of the said due date has been classified in the category of arrears. You now have 20 days to honor your commitments',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous constatons avec regret que le solde de votre compte n° {{NUM_COMPTE_CLIENT}} dans nos livres, n’a pas permis le règlement de l’échéance au {{DATE_ECHEANCE}}, de {{TOTAL_ECH}} FCFA du prêt de nominal {{MONTANT_PRET}} FCFA qui vous avait été accordé par notre Etablissement.

                    Le montant de ladite échéance a été classé dans la catégorie des impayés, conformément à la règlementation en vigueur, dans l’attente de votre couverture dans un délai de vingt jours, à compte de l’envoi de la présente.

                Dans cette attente, Veuillez agréer, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, l’expression de nos sentiments distingués.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We note with regret that the balance of your account n° {{NUM_COMPTE_CLIENT}} in our books, did not allow the payment of the due date at {{DATE_ECHEANCE}}, of FCFA {{TOTAL_ECH}} of the nominal loan FCFA {{MONTANT_PRET}} which had been granted to you by our Establishment.

                    The amount of the said due date has been classified in the category of unpaid bills, in accordance with the regulations in force, pending your coverage within twenty days. On account of the sending of this letter.

                In the meantime, Please accept, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, the expression of our distinguished feelings.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },

        // Deuxième lettre de relance client réseau
        {
            desc: 'Notification deuxième lettre de relance clients réseau',
            label: 'Notification deuxième lettre de relance (clients réseau)',
            templateType: 4,
            subjectFR: `[ALERITNG BCI] Deuxième lettre de relance`,
            subjectEN: `[ALERITNG BCI] Second letter of recovery`,
            clientProfile: 200,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J+30',
            sms: {
                frenchText: `{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, Suite à notre de relance du {{DATE_PRE_LETTRE}}  sans suite du crédit {{NUM_DOSSIER}}.  Vous disposez d'un délai de 8 jours pour régulariser cette situation. Passé ce délai, nous nous verrons contraints de transférer votre dossier à notre service du précontentieux`,
                englishText: '{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, Following our relaunch of {{DATE_PRE_LETTRE}} without continuation of credit {{NUM_DOSSIER}}. You have 8 days to rectify this situation. After this period, we will be forced to transfer your case to our pre-litigation department',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous nous référons à notre lettre de relance du {{DATE_PRE_LETTRE}} au sujet des échéances impayées de votre prêt nominal de {{MONTANT_PRET}} à ce jour dans nos livres.

                    Nous vous invitons à vous mettre en règle avec notre Etablissement, en régularisant ces impayés dans un délai de 8 jours, à compte de l’envoi de la présente lettre.

                    A l’expiration de ce délai, nous nous verrons contraints de transférer votre dossier à notre Service du Précontentieux.

                Dans l’attente de votre règlement, veuillez agréer, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, l’expression de nos sentiments distingués.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We refer to our reminder letter of {{DATE_PRE_LETTRE}} regarding the unpaid maturities of your nominal loan of {{MONTANT_PRET}} to date in our books.

                    We invite you to get in good standing with our Establishment, by settling these unpaid bills within 8 days. Due to the sending of this letter.

                    At the end of this period, we will be forced to transfer your case to our Pre-Litigation Department.

                Pending your settlement, please accept, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, the expression of our distinguished feelings.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },

        // Deuxième lettre de relance client corporate
        {
            desc: 'Notification deuxième lettre de relance clients corporate',
            label: 'Notification deuxième lettre de relance (clients corporate)',
            templateType: 4,
            subjectFR: `[ALERITNG BCI] Deuxième lettre de relance`,
            subjectEN: `[ALERITNG BCI] Second letter of recovery`,
            clientProfile: 100,
            operationCode: 'CREDITS',
            variables,
            dates: { 'created': 1601476686964, 'updated': 1601476686964 },
            notifyDate: 'J+30',
            sms: {
                frenchText: `{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, Suite à notre de relance du {{DATE_PRE_LETTRE}}  sans suite du crédit {{NUM_DOSSIER}}.  Vous disposez d'un délai de 8 jours pour régulariser cette situation. Passé ce délai, nous nous verrons contraints de transférer votre dossier à notre service du précontentieux`,
                englishText: '{{CIVILITE_CLIENT}} {{NOM_CLIENT}}, Following our relaunch of {{DATE_PRE_LETTRE}} without continuation of credit {{NUM_DOSSIER}}. You have 8 days to rectify this situation. After this period, we will be forced to transfer your case to our pre-litigation department',
            },
            email: {
                frenchText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    Nous nous référons à notre lettre de relance du {{DATE_PRE_LETTRE}} au sujet des échéances impayées de votre prêt nominal de {{MONTANT_PRET}} à ce jour dans nos livres.

                    Nous vous invitons à vous mettre en règle avec notre Etablissement, en régularisant ces impayés dans un délai de 8 jours. À compte de l’envoi de la présente lettre.

                    A l’expiration de ce délai, nous nous verrons contraints de transférer votre dossier à notre Service du Précontentieux.

                Dans l’attente de votre règlement, veuillez agréer, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, l’expression de nos sentiments distingués.`,
                englishText:
                `{{CIVILITE_CLIENT}} {{NOM_CLIENT}},

                    We refer to our reminder letter of {{DATE_PRE_LETTRE}} regarding the unpaid maturities of your nominal loan of {{MONTANT_PRET}} to date in our books.

                    We invite you to get in good standing with our Establishment, by settling these unpaid bills within 8 days. Due to the sending of this letter.

                    At the end of this period, we will be forced to transfer your case to our Pre-Litigation Department.

                Pending your settlement, please accept, {{CIVILITE_CLIENT}} {{NOM_CLIENT}}, the expression of our distinguished feelings.`,
            },
            author: 'John Doe',
            enabled: false,
            priority: 3
        },
    ];

    try {
        const { result } = await db.collection('templates_credits').insertMany(templates);
        logger.info(`${result.n} documents inserted in 'templates_credits' collection`);
    } catch (error) {
        logger.error(`default templates insertion failed \n${error.stack}`);
    }
}