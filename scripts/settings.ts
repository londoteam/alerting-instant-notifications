import { insertDefaultSettings } from './helpers/insert-default-settings';
import { dropSetting } from './helpers/reset-database';

(async () => {
    await dropSetting(true);
    await insertDefaultSettings();
    process.exit();
})();