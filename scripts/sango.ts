import { insertDefaultStatusinProcessSango } from './helpers/insert-default-status-inProcess-sango';

(async () => {
    await insertDefaultStatusinProcessSango();
    process.exit();
})();