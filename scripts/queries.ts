import { dropQueries } from './helpers/reset-database';
import { insertDefaultQueries } from './helpers/insert-default-queries';

(async () => {
    await dropQueries(true);
    await insertDefaultQueries();
    process.exit();
})();