FROM londotech/node-ts:latest

USER root

# Expose the port the app runs in
EXPOSE 4000

ENV NODE_PATH=/usr/lib/node_modules

COPY package.json /tmp/package.json

RUN cd /tmp && npm install --only=production --unsafe-perm=true

RUN mkdir -p /usr/src/instantaneous && cp -a /tmp/node_modules /usr/src/instantaneous/

WORKDIR /usr/src/instantaneous

COPY . /usr/src/instantaneous

# RUN NODE_ENV=staging-bci
CMD npm run start:staging-bci