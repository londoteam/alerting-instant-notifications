import { InsertOneWriteOpResult } from 'mongodb';
import { getDatabase } from './config';



const collectionName = 'notifications';

export const notifysCollection = {

    insertNotification: async (notification: any): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        if (!notification) { return; }
        return await database.collection(collectionName).insertOne(notification);
    },

}