import { config } from '../config';
import { MongoClient, MongoClientOptions, Db } from 'mongodb';

let database: Db = null;

export async function startDatabase() {
    const mongoDBURL = (config.get('db.auth.user') && config.get('db.auth.password'))
        ? `mongodb://${config.get('db.auth.user')}:${config.get('db.auth.password')}@${config.get('db.host')}/${config.get('db.name')}?retryWrites=true&w=majority`
        : `mongodb://${config.get('db.host')}/${config.get('db.name')}?retryWrites=true&w=majority`;

    const options: MongoClientOptions = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }

    if (config.get('db.auth.user') && config.get('db.auth.password')) {
        options.auth = {
            user: config.get('db.auth.user'),
            password: config.get('db.auth.password')
        }
    }

    const connection = await MongoClient.connect(mongoDBURL, options);
    database = connection.db();
}

export async function getDatabase(): Promise<Db> {
    if (!database) await startDatabase();
    return database;
}