export interface Attachment {
    file_name: string;
    file_content: string;
    file_content_type: string;
}
