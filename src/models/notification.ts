import { OperationCode } from "../enums/operations.enum";
import { Attachment } from "./attachment";

export interface Notification {
    message?: string;
    clientCode?: string;
    subject?: string;
    subjectFR?: string;
    subjectEN?: string;
    status?: number; // 100 (Created), 200 (Success Sent), 300 (Unsuccess Sent)
    type?: number;
    format?: number;
    smsCount?: number;
    priority?: number;
    clientProfile?: number;
    operationCode?: OperationCode;
    telephone?: string;
    dates?: { createdAt?: number, sendAt?: number };
    email?: string;
    source?: Source;
    htmlBody?: any;
    cc?: string;
    Attachments?: Attachment;
    accountId?: number;
}

export interface Source {
    client: string;
    app: string;
}