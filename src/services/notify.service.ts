import { config } from './../config';
import { logger } from '../winston';
import { helper } from './helpers/notify.helper';
import { notifysCollection } from '../collections/notify.collection';
import { isEmpty } from 'lodash';
import { commonService } from './common.service';



export const notifyService = {

    generateInstantSMS: async (params: any) => {

        let { phone, text, source } = params;

        if (!phone || !text || !source) { return new Error('MissingParams'); }
        phone = phone.replace('+', '');
        if (!commonService.checkIsCongonianNumber(phone)) { return new Error('BadPhoneFormatting'); }

        try {
            if (source.includes('IRIS')) {
                text = text.split('%2F').join('-'); text = text.split('%3A').join(':'); text = text.split('+').join(' ');
            }
            const smsNotification = helper.generateSMSNotication(phone, text, source);
            console.log('instant-sms notifications', smsNotification);
            await notifysCollection.insertNotification(smsNotification);
            return { status: 200, message: 'SMS snet' };
        } catch (error) {
            logger.error(`Instant SMS notification generation failed \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    generateInstantEmail: async (params: any) => {

        const { to, subject, body, source, attachments, priority, cc, accountId } = params;

        if (!to || !subject || !body || !source) { return new Error('MissingParameter'); }

        try {
            const emailNotification = helper.generateEmailNotication(to, subject, body, source, priority, attachments, cc, accountId);
            await notifysCollection.insertNotification(emailNotification);

        } catch (error) {
            logger.error(`Instant EMAIL notification generation failed \n${error.name} \n${error.stack}`);
            return error;
        }
    },

}
