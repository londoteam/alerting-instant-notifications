import { OperationCode } from '../../enums/operations.enum';
import { Notification } from '../../models/notification';
import { Attachment } from '../../models/attachment';
import { compile } from 'handlebars';
import { readFileSync } from 'fs';
import * as moment from 'moment';


const mailTemplate = readFileSync(__dirname + '/templates/notify-mail.template.html');
moment.locale('fr');

export const helper = {

    generateSMSNotication: (to: any, message: any, sourceApp: any) => {

        const smsCount = Math.ceil(message.length / 160);

        const sms: Notification = {
            message,
            smsCount,
            priority: 1,
            status: 100,
            format: 100,
            telephone: to,
            dates: { createdAt: moment().valueOf() },
            operationCode: OperationCode.INSTANT_NOTICATION,
            source: { client: 'BCI', app: sourceApp }
        }

        return sms;
    },

    generateEmailNotication: (to: any, subject: string, body: any, sourceApp: any, priority?: number, attachments?: Attachment[], cc?: string, accountId?: number) => {

        let htmlBody = body;
        const splitted = body.split(/\n/);
        body = splitted.filter((elt: any) => !!elt);
        if (sourceApp === 'WEB') {
            // generate htmlBody if request came from WEB
            const template = compile(mailTemplate.toString());
            htmlBody = template({ mailContent: body, date: `${moment().format('DD MMMM YYYY')}` });
        }

        const email: Notification = {
            subject,
            email: to,
            status: 100,
            format: 200,
            message: body,
            htmlBody,
            priority: priority ?? 1,
            dates: { createdAt: moment().valueOf() },
            operationCode: OperationCode.INSTANT_NOTICATION,
            source: { client: 'BCI', app: sourceApp },
            accountId
        }

        if (attachments && attachments instanceof Array && attachments?.length > 0) {
            const { file_name, file_content, file_content_type } = attachments[0];
            email.Attachments = { file_name, file_content, file_content_type };
        }

        if (cc) { email.cc = cc; }

        return email;
    },
}

