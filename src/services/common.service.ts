import { config } from '../config';
import { isString, isNumber } from 'lodash';

const service: any = {};

export const commonService = {

    parseNumberFields: (fields?: any) => {
        for (const key in fields) {
            if (!fields.hasOwnProperty(key)) { continue; }
            if (RegExp(/[a-z]/i).test(fields[key])) { continue; }
            if (key === 'clientCode') { continue; }
            if (key === 'queryFilter') { continue; }
            if (key.includes('niu')) { continue; }
            fields[key] = (isString(fields[key]) && /^[0-9]+$/.test(fields[key])) ? parseInt(fields[key], 10) : fields[key];
        }
    },

    timeout: (ms: number = 2000) => {
        if (config.get('env') !== 'development') { return; }
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    getRandomInt: (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    },

    getRandomString: (size: number, numberOnly?: boolean) => {
        size = size || 10;
        const chars = numberOnly ?
            '0123456789' :
            '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ';
        let randomstring = '';
        for (let i = 0; i < size; i++) {
            const rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        return randomstring;
    },

    generateErrResponse: (message: string, err: Error) => {
        const errResp: any = { message };

        if (config.get('env') !== 'production') {
            errResp.details = `${err.stack}`;
        }

        return errResp;
    },

    removeTraillingWhiteSpaces: (fields: any) => {
        for (const key in fields) {
            if (!fields.hasOwnProperty(key)) { continue; }
            if (isNumber(fields[key])) { continue; }
            fields[key] = `${fields[key]}`.trim();
        }
    },

    checkIsCongonianNumber: (number: any): boolean => {
        number = `${number}`.replace('+', '');
        if ( `${number}`.length !== 12) { return false; }
        // if ( `${number}`.length !== 12 || `${number}`.slice(0, 3) !== "242") { return false; }
        return true;
    }

}