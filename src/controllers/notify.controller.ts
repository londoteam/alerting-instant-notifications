import { Request, Response } from 'express';
import { config } from '../config';
import { commonService } from '../services/common.service';
import { notifyService } from '../services/notify.service';

export const notifyController = {

    init: (app: any): void => {

        app.post('/notify/instantaneous/generate/instant-sms', async (req: Request, res: Response) => {

            console.log('body instant-sms: ', req.body);
            const data: any = await notifyService.generateInstantSMS(req.body);

            if (data instanceof Error && data.message === 'MissingParameter') {
                const message = 'empty body recieved.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(400).json(errResp);
            }
            if (data instanceof Error && data.message === 'BadPhoneFormatting') {
                const message = 'bad phone format.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(400).json(errResp);
            }
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }

            await commonService.timeout(1000);

            res.send(data);
        });

        app.post('/notify/instantaneous/generate/instant-email', async (req: Request, res: Response) => {

            const data: any = await notifyService.generateInstantEmail(req.body);

            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            if (data instanceof Error && data.message === 'MissingParameter') {
                const message = 'Mandatory parameter are missing.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(400).json(errResp);
            }

            await commonService.timeout(1000);

            res.status(200).json(data);
        });
    }

}